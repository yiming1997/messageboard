﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessageBoard.Models
{
        public class User
        {
            public string UserName { get; set; }
            public string Account { get; set; }
            public string Password { get; set; }
            public string PhotoPath { get; set; }
        }
}