﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessageBoard.Models
{
    public class Message
    {
        public int? Id { get; set; }
        public string MessageContent { get; set; }
        public DateTime Time { get; set; }
        public string BelongAccount { get; set; }
    }
}