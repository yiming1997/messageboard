﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessageBoard.Models
{
    public class ValidationAccountToken
    {
        public string Account { get; set; }
        public string BelongAccount { get; set; }
    }
}