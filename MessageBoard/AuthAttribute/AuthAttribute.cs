﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using JWT;
using JWT.Serializers;
using MessageBoard.Models;
using System.Text;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using Newtonsoft.Json;
using System.IO;
using MessageBoard.DAL;
using System.Reflection;

namespace MessageBoard.AuthAttribute
{
    public class ApiAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            var text = new StreamReader(HttpContext.Current.Request.InputStream).ReadToEnd();
            string requestedAccount = GetAccount(text);
                       
            var authHeader = from t in actionContext.Request.Headers where t.Key == "auth" select t.Value.FirstOrDefault();
         
            if (authHeader != null)
            {
                const string secretKey = "Hello World";//加密秘钥
                string token = authHeader.FirstOrDefault();
                 if (!string.IsNullOrEmpty(token))
                 {
                    try
                      {
                            byte[] key = Encoding.UTF8.GetBytes(secretKey);
                            IJsonSerializer serializer = new JsonNetSerializer();
                            IDateTimeProvider provider = new UtcDateTimeProvider();
                            IJwtValidator validator = new JwtValidator(serializer, provider);
                            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
                            IJwtDecoder decoder = new JwtDecoder(serializer, validator, urlEncoder);
                            var json = decoder.DecodeToObject<AuthInfo>(token, key, verify: true);
                         
                            if (json != null)
                            {
                                if (json.ExpiryDateTime < DateTime.Now)
                                {
                                    return false;
                                }
                                if(json.Account!=requestedAccount)
                               {
                                    return false;              //此Token不属于该被请求的account
                                }
                                    
                                actionContext.RequestContext.RouteData.Values.Add("auth", json);
                                return true;
                            }
                            return false;
                        }
                        catch (Exception ex)
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }
            
            return false;
        }
    
        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            var erModel = new
            {
                Success = "false",
                ErrorCode = "401"
            };
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.OK, erModel, "application/json");
        }
        public string GetAccount(string text)
        {
            string account = "";
            var objValidationAccount = JsonConvert.DeserializeObject<ValidationAccountToken>(text);          
            if(objValidationAccount.Account!=null)
            {
                account = objValidationAccount.Account.Trim();
            }
            else
            {
                account = objValidationAccount.BelongAccount.Trim();
            }
            return account;
        }
    }
}
