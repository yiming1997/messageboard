﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MessageBoard.Models;
using System.Data;
using System.Data.SqlClient;

namespace MessageBoard.DAL
{
    public class UserLoginService
    {
        public string UserLogin(User objUser)
        {
            string sql = "select UserName from [Users] where Account='{0}'and Password='{1}'";
            sql = string.Format(sql, objUser.Account, objUser.Password);

            try
            {
                SqlDataReader objReader = SQLHelper.GetReader(sql);
                if (objReader.Read())
                {
                    objUser.UserName = objReader["UserName"].ToString();
                    return objUser.UserName;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("连接数据库失败" + ex);
            }

        }
    }
}