﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MessageBoard.Models;
using JWT;
using JWT.Algorithms;
using JWT.Serializers;
using System.Text;

namespace MessageBoard.DAL
{
    public class TokenService
    {
        public static TokenInfo CreateToken(AuthInfo authInfo)
        {
            TokenInfo tokenInfo = new TokenInfo();
            const string secretKey = "Hello World";
            try
            {
                byte[] key = Encoding.UTF8.GetBytes(secretKey);
                IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
                IJsonSerializer serializer = new JsonNetSerializer();
                IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
                IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);
                var token = encoder.Encode(authInfo, key);

                tokenInfo.Success = true;
                tokenInfo.Token = token;
                tokenInfo.Message = "OK";
            }
            catch (Exception ex)
            {
                tokenInfo.Success = false;
                tokenInfo.Message = ex.Message.ToString();
            }
            return tokenInfo;
        }
    }
}