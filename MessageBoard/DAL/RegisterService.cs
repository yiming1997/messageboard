﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using MessageBoard.Models;

namespace MessageBoard.DAL
{
    public class RegisterService
    {
        private static readonly string connString = ConfigurationManager.ConnectionStrings["SqlConnection"].ToString();

        public static bool CheckAccountExist(string account)
        {
            string sql = "select * from [Users] where Account = '{0}'";
            sql = string.Format(sql, account);

            try
            {
                SqlDataReader objReader = SQLHelper.GetReader(sql);
                if(objReader.Read())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("连接数据库失败" + ex);
            }
        }
        public static void Register(User user)
        {
            string sql = "insert into [Users](Account,Password,UserName) values('{0}','{1}',N'{2}')";
            sql = string.Format(sql, user.Account, user.Password, user.UserName);

            try
            {
                SQLHelper.Update(sql);
            }
            catch (Exception ex)
            {
                throw new Exception("连接到数据库失败" + ex);
            }
        }
        public static void SavePhoto(string account,string path)
        {
            string sql = "update [Users] set PhotoPath = '{0}'where Account = '{1}'";
            sql = string.Format(sql, path, account);

            try
            {
                SQLHelper.Update(sql);
            }
            catch (Exception ex)
            {
                throw new Exception("连接到数据库失败" + ex);
            }
        }
    }
}