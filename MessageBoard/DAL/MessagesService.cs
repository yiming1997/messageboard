﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using MessageBoard.Models;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace MessageBoard.DAL
{
    public class MessagesService
    {
        private static readonly string connString = ConfigurationManager.ConnectionStrings["SqlConnection"].ToString();

        public static List<Message> Query(string account)
        {
            using (IDbConnection connection = new SqlConnection(connString))
            {
                return connection.Query<Message>("select * from [Message] where BelongAccount = (@belongAccount)", new { belongAccount = account }).ToList();
            }
        }

        public static void Save(Message message)
        {
            string sql = "insert into [Message](MessageContent,Time,BelongAccount) values(N'{0}','{1}','{2}')";
            sql = string.Format(sql, message.MessageContent, message.Time, message.BelongAccount);

            try
            {
                SQLHelper.Update(sql);
            }
            catch (Exception ex)
            {
                throw new Exception("数据库连接发生错误" + ex);
            }
        }

        public static void Delete(int? id)
        {
            string sql = "delete from [Message] where Id = {0}";
            sql = string.Format(sql, id);

            try
            {
                SQLHelper.Update(sql);
            }
            catch (Exception ex)
            {
                throw new Exception("数据库连接发生错误" + ex);
            }
        }

    }
}