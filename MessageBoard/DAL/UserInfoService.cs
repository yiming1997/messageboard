﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using MessageBoard.Models;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace MessageBoard.DAL
{
    public class UserInfoService
    {
        private static readonly string connString = ConfigurationManager.ConnectionStrings["SqlConnection"].ToString();

        public static User GetUserInfo(User user)
        {
            string sql = "select * from [Users] where Account = '{0}'";
            sql = string.Format(sql,user.Account);

            try
            {
                SqlDataReader objReader = SQLHelper.GetReader(sql);
                if(objReader.Read())
                {
                    user.Account = objReader["Account"].ToString();
                    user.UserName = objReader["UserName"].ToString();
                    user.PhotoPath = objReader["PhotoPath"].ToString();

                    return user;
                }
                else
                {
                    return null;
                }
            }
            catch(Exception ex)
            {
                throw new Exception("连接数据库失败" + ex);
            }
        }
    }
}