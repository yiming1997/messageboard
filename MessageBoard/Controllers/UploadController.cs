﻿using MessageBoard.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using MessageBoard.DAL;

namespace MessageBoard.Controllers
{
    public class UploadController : ApiController
    {
        [HttpPost]
        public IHttpActionResult UploadFile()
        {
            HttpFileCollection files = HttpContext.Current.Request.Files;
            foreach (string key in files.AllKeys)
            {
                HttpPostedFile file1 = files[key];
                if(string.IsNullOrEmpty(file1.FileName)==false)
                {
                    string account = key;
                    string path = @"E:\HTML+CSS\untitled4";
                    //file1.SaveAs(HttpContext.Current.Server.MapPath("~/App_Data/")+ file1.FileName);
                    //file1.SaveAs(HttpContext.Current.Server.MapPath("D:/C# asp.mvc/"+key) + file1.FileName);
                    //file1.SaveAs(@"D:/C# asp.mvc/abc" + file1.FileName);
                     file1.SaveAs(Path.Combine(path,key+file1.FileName));
                     RegisterService.SavePhoto(account,key+file1.FileName);//由于前端img的src只能访问相对路径，所以用file1.FileName做路径;
                } 
            }
            return Ok("成功");
        }
    }
}
