﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MessageBoard.AuthAttribute;
using Newtonsoft.Json;
using Json;
using MessageBoard.DAL;
using MessageBoard.Models;
using System.Web.Http.Controllers;

namespace MessageBoard.Controllers
{
    // [TokenFilter]
    [ApiAuthorize]
    [AntiSqlInjection.AntiSqlInjectFilter]
    public class UserInfoController : ApiController
    {
        [HttpPost]
        public IHttpActionResult GetUserInfo(User user)
        {
            User objUser = UserInfoService.GetUserInfo(user);    
            
                if (objUser != null)
                {
                    return Ok(objUser);
                }
                else
                {
                    return Ok("获取用户信息失败");
                }
        }
    }
}
