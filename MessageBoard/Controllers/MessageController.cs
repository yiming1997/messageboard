﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MessageBoard.DAL;
using MessageBoard.Models;
using MessageBoard.AuthAttribute;
using MessageBoard.AntiSqlInjection;

namespace MessageBoard.Controllers
{
    [ApiAuthorize]
    public class MessageController : ApiController
    {
        [HttpPost]
        public IHttpActionResult SendMessages([FromBody]User user)
        {
            var messageList = MessagesService.Query(user.Account);

            return Ok(messageList);
        }

        [HttpPost]
        public IHttpActionResult SaveMessage([FromBody]Message message)
        {
            MessagesService.Save(message);

            return Ok("保存成功");
        }

        [HttpPost]
        public IHttpActionResult DeleteMessage([FromBody]Message message)
        {
            MessagesService.Delete(message.Id);

            return Ok("删除成功");
        }
    }
}
