﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using MessageBoard.AllowAttribute;
using MessageBoard.Models;
using MessageBoard.BLL;
using MessageBoard.DAL;

namespace MessageBoard.Controllers
{
    [AllowCrossSiteJson]
    public class LoginByJsonController : ApiController
    {
        [HttpPost]
        public IHttpActionResult Login(User objUser)
        {
            TokenInfo tokenInfo = new TokenInfo();
            string userName = new ManageUser().UserLogin(objUser);
            if (userName != null)
            {
                bool isAdmin = (userName == "admin") ? true : false;

                AuthInfo authInfo = new AuthInfo
                {
                    Account = objUser.Account,
                    Roles = new List<string>
                { "admin", "commonrole" },
                    IsAdmin = isAdmin,
                    ExpiryDateTime = DateTime.Now.AddHours(2)
                };
              tokenInfo = TokenService.CreateToken(authInfo);              
            }
            else
            {
                tokenInfo.Success = false;
                tokenInfo.Message = "用户信息为空";
            }

            return Ok(tokenInfo);
        }
    }
}
