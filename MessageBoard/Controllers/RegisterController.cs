﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MessageBoard.DAL;
using MessageBoard.Models;
using MessageBoard.AuthAttribute;
using MessageBoard.AntiSqlInjection;


namespace MessageBoard.Controllers
{
    public class RegisterController : ApiController
    {
        public IHttpActionResult CheckAccountExist([FromBody] User user)
        {
            string account = user.Account;
             if(!RegisterService.CheckAccountExist(account)) //如果账号不存在
            {
                return Ok("");
            }
             else                                            //如果账号已存在
            {
                return Ok("账号已存在");
            }
        }

        public IHttpActionResult Register([FromBody] User user)
        {
            RegisterService.Register(user);

            return Ok("注册成功！");
        }
    }
}
