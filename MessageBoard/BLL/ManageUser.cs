﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MessageBoard.DAL;
using MessageBoard.Models;

namespace MessageBoard.BLL
{
    public class ManageUser
    {
        public string UserLogin(User objUser)
        {
            if (new UserLoginService().UserLogin(objUser) != null)
            {
               // HttpContext.Current.Session["currentUser"] = objUser;
                return objUser.UserName;
            }
            else
            {
                return null;
            }
        }
    }
}